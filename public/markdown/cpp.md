
C++ Projects
============

---

Sorting Algorithm Visualizer
----------------------------


<img src="images/sorter.gif" width="240" height="240" />

This program uses object oriented programing to visualize a class that I designed to randomize and sort a simple set of integer data using a GUI. The purpose of this project is to be able to see exactly what it looks like for a particular algorithm to sort a set of data. This project was heavily inspired by Timo Bingmann's [Sounds of Sorting](http://panthema.net/2013/sound-of-sorting/), which is a much cooler project by the way. Check it out!

---

Double Pendulum Visualizer
--------------------------


<img src="images/double_pendulum.gif" width="240" height="240" />

This program visualizes how a double pendulum moves under varying conditions, which can be manipulated with the GUI. The key to making this project work correctly was to increment the angles by the correct amount depending on the position and previous speed of the system. This involves analysis of the angular acceleration of the system. Thankfully, the folks at [myphysicslab.com](https://www.myphysicslab.com/pendulum/double-pendulum-en.html) already figured this out for me. If you scroll down to the 'Direct Method for Finding Equations of Motion' section, you can find the equations I used to make this motion work. It is then as simple as incrementing the velocity by the amount of acceleration and likewise incrementing the position by the amount of velocity. I also constrained the angles so the doubles do not overflow (the numbers shockingly large very quickly).