![Build Status](https://gitlab.com/pages/plain-html/badges/master/build.svg)

---

#### www.benjburger.com

My plain HTML site using GitLab Pages. I'll be posting my projects or really anything else
here.

Learn more about GitLab Pages at https://pages.gitlab.io and the official
documentation https://docs.gitlab.com/ce/user/project/pages/.

---

I built this website using [bootstrap](https://getbootstrap.com) and [showdown.js](https://github.com/showdownjs/showdown). 
This project is more of an excercise for me to test out interfacing js, html, python, and markdown. 

**Workflow:** 

I write each page in markdown, which is tranlated to HTML by the script, `build.py`, which can be found in the `public` 
folder. That translated markdown is then just inserted to the pages by a jquery load function.

I might never use this website or it might become something interesting. Not sure. Stay tuned.

