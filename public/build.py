#!/usr/local/bin/python3

import sys
import os
from pathlib import Path

args = sys.argv[1:]

def usage(status=0):
    print('''Usage: {} [Target]
    
    [Target]:   "all" or one "filename"
    '''.format(os.path.basename(sys.argv[0])))
    sys.exit(status)


if len(args) != 1:
    usage(1)

arg = args[0]

if arg == "all":
    files = [f for f in os.listdir('markdown')]
    
    for item in files:
        if item.endswith(".md"):
            in_file = item
            out_file = item[:-3] + "_markdown.html"
            print("Building " + in_file + " as " + out_file + ":")
            os.system("showdown makehtml -i markdown/" + in_file + " -o markdown_html/" + out_file)
else:
    if Path(arg).is_file():
        if arg.endswith(".md"):
            in_file = arg
            out_file = arg[:-3] + "_markdown.html"
            os.system("showdown makehtml -i " + in_file + " -o markdown_html/" + out_file)
        else:
            print("Not a markdown file!")
            sys.exit(1)
    else:
        print("File does not exist!")
        sys.exit(1)
