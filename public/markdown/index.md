Welcome to my Project Site!
===========================

---

My name is Ben Burger. Thanks for checking out my project site.

You can navigate this site by clicking the links above. I currently am hosting projects deployed in c++, python, and bash.

This website is served using [Gitlab Pages](https://about.gitlab.com/product/pages/). Check out my [website repository](https://gitlab.com/bburger11/bburger11.gitlab.io), where I explain how I build this website using simple markdown documents.

---

```python
# I'm testing stuff, ignore:
print(x for x in test)

for i in range(1):  
  print('Single tab in')
  if True:
    print('Two tabs in')
```


`os.system("This is one line of code")`


<div class="alert alert-info" role="alert">
  <h6 class="alert-heading"><img src="images/gitlab.png" height="24" width="24">&nbsp;Check out this project on Gitlab</h6>
  <hr>
  <p>There should be information here</p>
</div>

<div class="alert alert-success" role="alert">
  <h4 class="alert-heading">Well done!</h4>
  <p>Aww yeah, you successfully read this important alert message. This example text is going to run a bit longer so that you can see how spacing within an alert works with this kind of content.</p>
  <hr>
  <p class="mb-0">Whenever you need to, be sure to use margin utilities to keep things nice and tidy.</p>
</div>
